import { query } from "../dbconfig/dbhelper";
import cryptoRandomString = require("crypto-random-string");

class UrlShortenerController {
  public async get(req, res) {
    try {
      const sql = `SELECT original_url FROM urls where short_url = '${req.params.identificador}' AND current_timestamp < expiration_date`;
      const rows = await query(sql);

      if (rows[0]) {
        const url = rows[0].original_url;
        res.redirect(url.startsWith("http") ? url : `https://${url}`);
      } else {
        res.status(404).send();
      }
    } catch (error) {
      console.log({ error });
      res.status(400).send(error);
    }
  }

  public async post(req, res) {
    try {
      let randomLength = Math.floor(Math.random() * (10 - 5 + 1) + 5);

      const sql = `INSERT INTO urls (original_url, short_url, expiration_date) VALUES ('${req.body.url.trim()}', '${cryptoRandomString(
        {
          length: randomLength,
          type: "alphanumeric",
        }
      )}', to_timestamp(${
        new Date().getTime() + 300000
      }/1000)) RETURNING short_url`;
      const rows = await query(sql);

      res.send({
        newUrl: `http://localhost:8081/${rows[0].short_url}`,
      });
    } catch (error) {
      console.log({ error });
      res.status(400).send(error);
    }
  }
}

export default UrlShortenerController;
