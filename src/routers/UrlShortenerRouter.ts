import express, { Router } from "express";
import UrlShortenerController from "../controllers/UrlShortenerController";

const router = Router();
const urlShortenerController = new UrlShortenerController();

router.get("/:identificador", urlShortenerController.get);
router.post("/encurtador", urlShortenerController.post);

export default router;
