import pool from "./dbconnector";

export async function query(sql: string) {
  const client = await pool.connect();
  const { rows } = await client.query(sql);
  client.release();
  return rows;
}
