import { Pool } from "pg";

const pool = new Pool({
  max: 20,
  connectionString:
    "postgres://flavioaugusto:wiserrocks@postgres:5432/urlshortener-db",
  idleTimeoutMillis: 30000,
});

export default pool;
