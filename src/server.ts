import express, { Application } from "express";
import bodyParser from "body-parser";
import urlShortenerRouter from "./routers/UrlShortenerRouter";
import pool from "./dbconfig/dbconnector";

class Server {
  private app: Application;

  constructor() {
    this.app = express();
    this.config();
    this.routerConfig();
    this.dbConnect();
  }

  private config() {
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: "1mb" })); // 100kb default
  }

  private dbConnect() {
    pool.connect(function (err: string) {
      if (err) throw new Error(err);
      console.log("Connected");
    });
  }

  private routerConfig() {
    this.app.use("/", urlShortenerRouter);
  }

  public start = (port: number) => {
    return new Promise((resolve, reject) => {
      this.app
        .listen(port, () => {
          resolve(port);
        })
        .on("error", (err: Object) => reject(err));
    });
  };
}

export default Server;
