CREATE TABLE urls
(
  id SERIAL PRIMARY KEY,
  original_url text NOT NULL,
  short_url text NOT NULL,
  expiration_date timestamp NOT NULL
)