# Challenge Encurtador - Backend

Endpoint que reproduz o comportamento padrão de um encurtador de URLs, gerando, armazenando e retornando endereços.

---

## Stack

- **Typescript**

- **NodeJS**

- **Express**

- **PostgreSQL**

- **Docker**

- **Postman** (documentação)

---

## Iniciando

### Rotas da Aplicação

- **`POST /encurtador`**: Rota para cadastrar uma URL completa e obter como retorno uma URL encurtada equivalente. O corpo da requisição deve conter um JSON como no exemplo a seguir: { url: "https://www.google.com" }

- **`GET /:identificador`**: Rota para obter a URL completa ao passar o identificador gerado quando a URL foi encurtada. O identificador sempre é uma string alfanumérica entre que possui entre 5 e 10 caracteres. Redireciona automaticamente o usuário, e caso não encontre uma URL correspondente, retorna status 404.

Mais info: `https://app.swaggerhub.com/apis/MeMyselfAndI83/url-shortener/1.0.0`

---

## Uso

Instale as dependências

```sh
npm install
```

Suba os containers do Docker

```sh
docker-compose up --build -d
```

**Voilà! O endpoint estará disponível em `localhost:8081`**

Obs.: Cada URL encurtada expira após 5 minutos.
